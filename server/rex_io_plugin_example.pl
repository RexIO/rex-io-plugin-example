#!/usr/bin/env perl
use Mojolicious::Lite;
use Mojo::UserAgent;
use Data::Dumper;

my $rex_io_server  = "http://localhost:5000";
my $my_domain      = "http://localhost:7000";
my $my_plugin_name = "helloworld";

### Register plugin into Rex.IO Server
my $ua = Mojo::UserAgent->new;
my $register_conf = {
  name    => $my_plugin_name,
  methods => [
    {
      url      => "/hello",
      meth     => "GET",
      auth     => Mojo::JSON->false,
      location => "$my_domain/1.0/hello",
    }
  ],
  hooks => {
    consume => [
      {
        plugin   => "user",
        action   => "/user",
        location => "$my_domain/1.0/user",
      },
    ],
  },
};

$ua->post("$rex_io_server/1.0/plugin/plugin", json => $register_conf);
### END

get '/' => sub {
  my $c = shift;
  $c->render('index');
};

post '/1.0/user' => sub {
  my $c = shift;
  my $ref = $c->req->json;
  
  $c->app->log->debug(Dumper($ref));
  
  # appending some own data to the user list
  if($ref->[0] eq "json") {
    push @{ $ref->[1]->{data} }, {
      permission_set_id => 1,
      password => '',
      group => {
        name => 'root',
        id => 1,
      },
      group_id => 1,
      name => 'toor',
      permission_set => {
        name => 'root',
        id => 1,
        description => '',
      },
      id => 17,
    };
  }
  
  $c->render(json => $ref);
};

get '/1.0/hello' => sub {
  my $c = shift;
  $c->render( json => { motd => 'Hello world!' } );
};

app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Welcome';
This is an example (server) plugin for Rex.IO.

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
